package com.vitalyorlov.testtaskforbs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.vitalyorlov.testtaskforbs.adapter.OffersRvAdapter;
import com.vitalyorlov.testtaskforbs.model.Offer;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class OffersActivity extends AppCompatActivity {

    private RecyclerView rv;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rv = (RecyclerView) findViewById(R.id.offers_rv);

        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        int categoryId = intent.getIntExtra("categoryId", 0);
        List<Offer> offersForCategory = new ArrayList<Offer>();

        for (Offer o: ListContainer.getOffers()) {
            if (o.getCategoryId() == categoryId)
                offersForCategory.add(o);
        }

        mAdapter = new OffersRvAdapter(offersForCategory, this);
        rv.setAdapter(mAdapter);
        rv.setHasFixedSize(true);

    }

}
