package com.vitalyorlov.testtaskforbs.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vitalyorlov.testtaskforbs.R;
import com.vitalyorlov.testtaskforbs.adapter.CategoriesRvAdapter;
import com.vitalyorlov.testtaskforbs.model.Category;

import java.util.ArrayList;

public class DownloadFragment extends Fragment {

    public DownloadFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_download, container, false);

        return v;
    }
}
