package com.vitalyorlov.testtaskforbs.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vitalyorlov.testtaskforbs.R;

public class ContactsFragment extends Fragment  {

//    static final LatLng TutorialsPoint = new LatLng(21 , 57);
//    private GoogleMap googleMap;

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_contacts, container, false);

//        try {
//            if (googleMap == null) {
//                ((MapFragment) this.getActivity().getFragmentManager().
//                        findFragmentById(R.id.map)).getMapAsync(this);
//            }
//            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//            Marker TP = googleMap.addMarker(new MarkerOptions().
//                    position(TutorialsPoint).title("TutorialsPoint"));
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }

        return v;
    }

//    @Override
//    public void onMapReady(final GoogleMap map) {
//        this.googleMap = map;
//    }

}
