package com.vitalyorlov.testtaskforbs.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vitalyorlov.testtaskforbs.R;
import com.vitalyorlov.testtaskforbs.adapter.CategoriesRvAdapter;
import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class CategoriesFragment extends Fragment {

    private RecyclerView rv;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Category> categories;

    public CategoriesFragment() {
        if (categories == null) new ArrayList<Category>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_categories, container, false);

        rv = (RecyclerView) v.findViewById(R.id.categories_rv);

        mLayoutManager = new LinearLayoutManager(this.getContext());
        rv.setLayoutManager(mLayoutManager);

        mAdapter = new CategoriesRvAdapter(categories, this.getActivity());
        rv.setAdapter(mAdapter);
        rv.setHasFixedSize(true);

        if (categories.size() == 0) {
            TextView tv = (TextView) v.findViewById(R.id.message);
            tv.setText("Failed to download categories");
        }

        return v;
    }

    public void setRecyclerViewAdapter(List<Category> categoryList) {
        mAdapter = new CategoriesRvAdapter(categoryList, this.getActivity());
        if (rv != null)
            rv.setAdapter(mAdapter);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
