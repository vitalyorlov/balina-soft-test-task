package com.vitalyorlov.testtaskforbs;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.vitalyorlov.testtaskforbs.adapter.CategoriesRvAdapter;
import com.vitalyorlov.testtaskforbs.fragment.CategoriesFragment;
import com.vitalyorlov.testtaskforbs.fragment.ContactsFragment;
import com.vitalyorlov.testtaskforbs.fragment.DownloadFragment;
import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.model.CategoryObject;
import com.vitalyorlov.testtaskforbs.model.DataResponse;
import com.vitalyorlov.testtaskforbs.model.Offer;
import com.vitalyorlov.testtaskforbs.model.OfferObject;
import com.vitalyorlov.testtaskforbs.tasks.GetXMLTask;
import com.vitalyorlov.testtaskforbs.tasks.SaveDataTask;
import com.vitalyorlov.testtaskforbs.utils.ApiService;
import com.vitalyorlov.testtaskforbs.utils.CacheService;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Callback<DataResponse> {

    //private static String keyValue = "ukAXxeJYZN";
    private static String url = "http://ufa.farfor.ru";
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragment = new DownloadFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.catalog) {
            fragment = new CategoriesFragment();
        } else if (id == R.id.contacts) {
            startActivity(new Intent(this, ContactsActivity.class));
        }

        this.setFragment(fragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        fragment = new CategoriesFragment();

        CacheService.loadData(this);

        if (ListContainer.getCategories() == null || ListContainer.getCategories().size() == 0) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(new OkHttpClient())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();

            ApiService apiService = retrofit.create(ApiService.class);
            Call<DataResponse> call = apiService.getResponse();
            call.enqueue(this);
        } else {
            CategoriesFragment fragment = new CategoriesFragment();
            this.setFragment(fragment);
            fragment.setCategories(ListContainer.getCategories());
            fragment.setRecyclerViewAdapter(ListContainer.getCategories());
        }

    }

    @Override
    public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
        int code = response.code();
        if (code == 200) {
            DataResponse dataResponse = response.body();

            ListContainer.setCategories(new ArrayList<Category>());
            for (CategoryObject categoryObject : dataResponse.shop.categories.categoriesList) {
                ListContainer.getCategories().add(new Category(categoryObject.id, categoryObject.text));
            }

            ListContainer.setOffers(new ArrayList<Offer>());
            for (OfferObject offerObject : dataResponse.shop.offers.offersList) {
                ListContainer.getOffers().add(new Offer(offerObject));
            }

            SaveDataTask saveDataTask = new SaveDataTask(this);
            saveDataTask.execute();
        } else {
            Toast.makeText(this, "Error getting data: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }

        CategoriesFragment fragment = new CategoriesFragment();
        this.setFragment(fragment);
        fragment.setCategories(ListContainer.getCategories());
        fragment.setRecyclerViewAdapter(ListContainer.getCategories());
    }

    @Override
    public void onFailure(Call<DataResponse> call, Throwable t) {
        Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    public void setFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        }
    }

}
