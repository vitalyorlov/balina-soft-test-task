package com.vitalyorlov.testtaskforbs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vitalyorlov.testtaskforbs.adapter.OffersRvAdapter;
import com.vitalyorlov.testtaskforbs.model.Offer;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class OfferExtendedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_extended);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        int offerId = intent.getIntExtra("offerId", 0);

        Offer offer = new Offer();

        for (Offer o: ListContainer.getOffers()) {
            if (o.getId() == offerId) {
                offer = o;
                break;
            }
        }

        TextView offerName = (TextView)findViewById(R.id.name);
        offerName.setText(offer.getName());

        TextView offerWeight = (TextView)findViewById(R.id.weight);
        offerWeight.setText(offer.getParams().containsKey("Вес") ? offer.getParams().get("Вес") : "");

        TextView offerPrice = (TextView)findViewById(R.id.price);
        offerPrice.setText(offer.getPrice());

        TextView offerDescription = (TextView)findViewById(R.id.description);
        offerDescription.setText(offer.getDescription());

        ImageView offerPhoto = (ImageView)findViewById(R.id.picture);
        Picasso.with(this)
                .load(offer.getPicture())
                .fit()
                .centerInside()
                .into(offerPhoto);

    }

}
