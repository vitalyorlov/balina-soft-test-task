package com.vitalyorlov.testtaskforbs.model;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "yml_catalog", strict = false)
public class DataResponse {

    @Element(name = "shop")
    public Shop shop;

}
