package com.vitalyorlov.testtaskforbs.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;

@Element(name = "category")
public class CategoryObject {

    @Attribute(name = "id")
    public String id;

    @Text
    public String text;

}
