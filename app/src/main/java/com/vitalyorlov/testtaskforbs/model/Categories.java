package com.vitalyorlov.testtaskforbs.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;

import java.util.List;

@Element(name = "categories")
@Order(elements = "category")
public class Categories {
    @ElementList(entry = "category", inline = true)
    public List<CategoryObject> categoriesList;
}
