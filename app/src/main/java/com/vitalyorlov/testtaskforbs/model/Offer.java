package com.vitalyorlov.testtaskforbs.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Offer implements Serializable {

    private int id;
    private String url;
    private String name;
    private String description;
    private String price;
    private String picture;
    private int categoryId;
    private Map<String, String> params;

    public Offer() {
        params = new HashMap<>();
    }

    public Offer(OfferObject offerObject) {
        this.id = Integer.valueOf(offerObject.id);
        this.url = offerObject.url;
        this.name = offerObject.name;

        if (offerObject.description != null)
            this.description = offerObject.description;
        else
            this.description = "";

        this.price = offerObject.price;

        if (offerObject.picture != null)
            this.picture = offerObject.picture;
        else
            this.picture = "";

        this.categoryId = Integer.valueOf(offerObject.categoryId);

        this.params = new HashMap<>();
        if (offerObject.paramsList != null) {
            for (Param p : offerObject.paramsList) {
                params.put(p.name, p.text);
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
