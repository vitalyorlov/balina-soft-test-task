package com.vitalyorlov.testtaskforbs.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;

import java.util.List;

@Element(name = "offers")
@Order(elements = "offer")
public class Offers {
    @ElementList(entry = "offer", inline = true)
    public List<OfferObject> offersList;
}
