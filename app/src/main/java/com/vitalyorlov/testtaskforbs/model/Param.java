package com.vitalyorlov.testtaskforbs.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;

@Element(name = "category")
public class Param {

    @Attribute(name = "name")
    public String name;

    @Text
    public String text;

}
