package com.vitalyorlov.testtaskforbs.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

@Element(name = "offer")
public class OfferObject {
    @Attribute(name = "id")
    public String id;

    @Element(name = "url")
    public String url;

    @Element(name = "name")
    public String name;

    @Element(name = "description", required = false)
    public String description;

    @Element(name = "price")
    public String price;

    @Element(name = "picture", required = false)
    public String picture;

    @Element(name = "categoryId")
    public String categoryId;

    @ElementList(entry = "param", inline = true, required = false)
    public List<Param> paramsList;

}
