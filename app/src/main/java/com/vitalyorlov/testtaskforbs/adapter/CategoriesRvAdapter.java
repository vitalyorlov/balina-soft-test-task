package com.vitalyorlov.testtaskforbs.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vitalyorlov.testtaskforbs.MainActivity;
import com.vitalyorlov.testtaskforbs.OffersActivity;
import com.vitalyorlov.testtaskforbs.R;
import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.model.Offer;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CategoriesRvAdapter extends RecyclerView.Adapter<CategoriesRvAdapter.CategoriesViewHolder> {

    public static class CategoriesViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView categoryName;
        ImageView categoryPhoto;
        int categoryId;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            categoryName = (TextView)itemView.findViewById(R.id.category_name);
            categoryPhoto = (ImageView)itemView.findViewById(R.id.category_photo);
        }
    }

    List<Category> categories;
    Activity context;

    public CategoriesRvAdapter(List<Category> categories, Activity context){
        this.categories = categories;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_item, viewGroup, false);
        CategoriesViewHolder cvh = new CategoriesViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final CategoriesViewHolder categoryViewHolder, int i) {
        categoryViewHolder.categoryName.setText(categories.get(i).getName());
        categoryViewHolder.categoryPhoto.setImageResource(R.drawable.ic_menu_gallery);
        categoryViewHolder.categoryId = categories.get(i).getId();
        categoryViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OffersActivity.class);
                intent.putExtra("categoryId", categoryViewHolder.categoryId);
                ((MainActivity)context).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}

