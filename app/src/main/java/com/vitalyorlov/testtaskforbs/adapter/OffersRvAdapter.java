package com.vitalyorlov.testtaskforbs.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vitalyorlov.testtaskforbs.MainActivity;
import com.vitalyorlov.testtaskforbs.OfferExtendedActivity;
import com.vitalyorlov.testtaskforbs.OffersActivity;
import com.vitalyorlov.testtaskforbs.R;
import com.vitalyorlov.testtaskforbs.model.Offer;

import java.util.List;

public class OffersRvAdapter extends RecyclerView.Adapter<OffersRvAdapter.OffersViewHolder> {

    public static class OffersViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView offerName;
        TextView offerWeight;
        TextView offerPrice;
        ImageView offerPhoto;
        int offerId;

        OffersViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            offerName = (TextView)itemView.findViewById(R.id.offer_name);
            offerWeight = (TextView)itemView.findViewById(R.id.offer_weight);
            offerPrice = (TextView)itemView.findViewById(R.id.offer_price);
            offerPhoto = (ImageView)itemView.findViewById(R.id.offer_photo);
        }
    }

    List<Offer> offers;
    Activity context;

    public OffersRvAdapter(List<Offer> offers, Activity context){
        this.offers = offers;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public OffersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offer_item, viewGroup, false);
        OffersViewHolder cvh = new OffersViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final OffersViewHolder offerViewHolder, int i) {
        offerViewHolder.offerName.setText(offers.get(i).getName());
        offerViewHolder.offerPrice.setText(String.valueOf(offers.get(i).getPrice()));
        offerViewHolder.offerId = offers.get(i).getId();
        if(offers.get(i).getParams().containsKey("Вес"))
            offerViewHolder.offerWeight.setText(offers.get(i).getParams().get("Вес"));
        Picasso.with(context)
                .load(offers.get(i).getPicture())
                .resize(100, 100)
                .centerCrop()
                .into(offerViewHolder.offerPhoto);
        offerViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OfferExtendedActivity.class);
                intent.putExtra("offerId", offerViewHolder.offerId);
                ((OffersActivity)context).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }
}

