package com.vitalyorlov.testtaskforbs.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.vitalyorlov.testtaskforbs.utils.CacheService;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;

public class SaveDataTask extends AsyncTask<Void, Void, Void> {

    private Activity context;

    public SaveDataTask(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        CacheService.saveData(context);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Toast.makeText(context, "Data is saved", Toast.LENGTH_SHORT).show();
    }
}
