package com.vitalyorlov.testtaskforbs.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.vitalyorlov.testtaskforbs.MainActivity;
import com.vitalyorlov.testtaskforbs.R;
import com.vitalyorlov.testtaskforbs.adapter.CategoriesRvAdapter;
import com.vitalyorlov.testtaskforbs.fragment.CategoriesFragment;
import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.model.Offer;
import com.vitalyorlov.testtaskforbs.utils.ListContainer;
import com.vitalyorlov.testtaskforbs.utils.XMLDOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetXMLTask extends AsyncTask<String, Void, String> {
    private Activity context;
    private List<Category> categories;
    private List<Offer> offers;
    private CategoriesFragment fragment;

    // XML node names
    static final String ATTR_ID = "id";
    static final String ATTR_NAME = "name";
    static final String NODE_CATEGORY = "category";
    static final String NODE_OFFER = "offer";
    static final String NODE_NAME = "name";
    static final String NODE_URL = "url";
    static final String NODE_PRICE = "price";
    static final String NODE_DESCRIPTION = "description";
    static final String NODE_PICTURE = "picture";
    static final String NODE_PARAM = "param";
    static final String NODE_CATEGORY_ID = "categoryId";

    public GetXMLTask(Activity context, List<Category> categories, List<Offer> offers, CategoriesFragment fragment) {
        this.context = context;
        this.categories = categories;
        this.offers = offers;
        this.fragment = fragment;
    }

    @Override
    protected String doInBackground(String... urls) {
        String xml = null;
        for (String url : urls) {
            xml = getXmlFromUrl(url);
        }
        return xml;
    }

    @Override
    protected void onPostExecute(String xml) {
        XMLDOMParser parser = new XMLDOMParser();
        InputStream stream;
        try {
            stream = new ByteArrayInputStream(xml.getBytes("UTF-16"));
        } catch (UnsupportedEncodingException ex) {
            return;
        }

        Document doc;
        try {
            doc = parser.getDocument(stream);
        } catch (Exception ex) {
            return;
        }

        NodeList nodeList = doc.getElementsByTagName(NODE_CATEGORY);

        Category category;
        for (int i = 0; i < nodeList.getLength(); i++) {
            category = new Category();
            Element e = (Element) nodeList.item(i);
            category.setId(Integer.parseInt(e.getAttribute(ATTR_ID)));
            category.setName(e.getTextContent());
            categories.add(category);
        }

        nodeList = doc.getElementsByTagName(NODE_OFFER);

        Offer offer = null;
        for (int i = 0; i < nodeList.getLength(); i++) {
            offer = new Offer();
            Element e = (Element) nodeList.item(i);
            offer.setId(Integer.parseInt(e.getAttribute(ATTR_ID)));
            offer.setName(parser.getValue(e, NODE_NAME));
            offer.setPrice(parser.getValue(e, NODE_PRICE));
            offer.setUrl(parser.getValue(e, NODE_URL));
            offer.setDescription(parser.getValue(e, NODE_DESCRIPTION));
            offer.setPicture(parser.getValue(e, NODE_PICTURE));
            offer.setCategoryId(Integer.valueOf(parser.getValue(e, NODE_CATEGORY_ID)));

            NodeList params = ((Element) nodeList.item(i)).getElementsByTagName(NODE_PARAM);
            for (int j = 0; j < params.getLength(); j++) {
            //    offer.getParams().put(params.item(j))
            }

            offers.add(offer);
        }

        ((MainActivity)context).setFragment(fragment);
        fragment.setCategories(categories);
        fragment.setRecyclerViewAdapter(categories);
        ListContainer.setOffers(offers);
        ListContainer.setCategories(categories);

    }

    private String getXmlFromUrl(String urlString) {
        OkHttpClient client = new OkHttpClient();

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException ex) {
            return null;
        }
    }

}
