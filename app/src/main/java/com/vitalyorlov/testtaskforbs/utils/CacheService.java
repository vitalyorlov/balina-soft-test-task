package com.vitalyorlov.testtaskforbs.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.model.Offer;

import java.util.ArrayList;
import java.util.List;

public class CacheService {
    static SharedPreferences sPref;

    static final String CATEGORIES = "categories";
    static final String OFFERS = "offers";

    public static void saveData(Activity context) {
        sPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(CATEGORIES, ObjectSerializer.serialize((ArrayList<Category>)ListContainer.getCategories()));
        ed.putString(OFFERS, ObjectSerializer.serialize((ArrayList<Offer>)ListContainer.getOffers()));
        ed.commit();
    }

    public static void loadData(Activity context) {
        sPref = context.getPreferences(Context.MODE_PRIVATE);
        ListContainer.setCategories((ArrayList<Category>) ObjectSerializer.deserialize(
                sPref.getString(CATEGORIES, ObjectSerializer.serialize(new ArrayList<Category>()))));
        ListContainer.setOffers((ArrayList<Offer>) ObjectSerializer.deserialize(
                sPref.getString(OFFERS, ObjectSerializer.serialize(new ArrayList<Offer>()))));
    }
}
