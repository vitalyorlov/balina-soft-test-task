package com.vitalyorlov.testtaskforbs.utils;

import com.vitalyorlov.testtaskforbs.model.Category;
import com.vitalyorlov.testtaskforbs.model.Offer;

import java.util.List;

public class ListContainer {
    private static List<Category> categories;
    private static List<Offer> offers;

    public static List<Category> getCategories() {
        return categories;
    }

    public static void setCategories(List<Category> categoriesList) {
        ListContainer.categories = categoriesList;
    }

    public static List<Offer> getOffers() {
        return offers;
    }

    public static void setOffers(List<Offer> offers) {
        ListContainer.offers = offers;
    }
}
