package com.vitalyorlov.testtaskforbs.utils;

import com.vitalyorlov.testtaskforbs.model.DataResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public interface ApiService {
    @GET("getyml/?key=ukAXxeJYZN")
    Call<DataResponse> getResponse();
}
